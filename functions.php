<?php


// Init theme functions.
require_once( get_template_directory() . '/includes/theme-init.php');


// Theme functions.
require_once( get_template_directory() . '/includes/theme-functions.php');


// Init WooCommerce functions.
// require_once( get_template_directory() . '/includes/woocommerce/woocommerce-init.php');


// WooCommerce functions.
// require_once( get_template_directory() . '/includes/woocommerce/woocommerce-functions.php');


?>