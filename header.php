<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">

        <meta name="viewport" content="width=device-width"; initial-scale="1">

        <title><?php wp_title(); ?></title>

        <meta name="msapplication-square70x70logo" content="<?php bloginfo('template_url'); ?>/assets/img/favicons/windows-tile-70x70.png">
        <meta name="msapplication-square150x150logo" content="<?php bloginfo('template_url'); ?>/assets/img/favicons/windows-tile-150x150.png">
        <meta name="msapplication-square310x310logo" content="<?php bloginfo('template_url'); ?>/assets/img/favicons/windows-tile-310x310.png">
        <meta name="msapplication-TileImage" content="<?php bloginfo('template_url'); ?>/assets/img/favicons/windows-tile-144x144.png">
        <meta name="msapplication-TileColor" content="#000000">
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon-152x152-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon-120x120-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon-76x76-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon-60x60-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/apple-touch-icon.png">
        <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/favicon.ico">
        <link rel="icon" type="image/png" sizes="64x64" href="<?php bloginfo('template_url'); ?>/assets/img/favicons/favicon.png">

        <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <!-- .m-page-head -->
        <header class="m-page-head">

            <div class="m-page-head__item">
                <a class="m-branding" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">

                    <?php if(get_field('logo_mobile')) : ?>
                        <?php $logo_mobile = get_field('logo_mobile', 'option'); ?>
                        <img alt="<?php echo $logo_mobile['alt']; ?>" src="<?php echo $logo_mobile['url']; ?>" title="<?php echo $logo_mobile['title']; ?>">
                    <?php endif; ?>

                    <?php $logo = get_field('logo', 'option'); ?>
                    <img alt="<?php echo $logo['alt']; ?>" src="<?php echo $logo['url']; ?>" title="<?php echo $logo['title']; ?>">

                </a>
            </div>


            <div class="m-page-head__item">

                <?php

                    $defaults = array(
                        'theme_location'  => 'header',
                        'menu'            => 'Primary Nav',
                        'container'       => 'nav',
                        'container_class' => 'm-site-nav',
                        'container_id'    => 'nav',
                        'menu_class'      => '',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 2,
                        'walker'          => ''
                    );

                    wp_nav_menu( $defaults );

                ?>

            </div>


            <div class="m-page-head__item">
                <a href="#" id="nav-toggle">
                    <span class="u-hide">menu</span>
                    <i class="icon-menu"></i>
                </a>
            </div>

        </header>
