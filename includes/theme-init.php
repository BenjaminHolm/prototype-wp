<?php


// Clean up WordPress.
add_filter('the_generator','remove_generators');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


// CSS.
add_action('wp_enqueue_scripts', 'theme_css');


// JS - custom js in footer.
// add_action('wp_footer', 'footer_scripts');


// JS - Gravity Forms js in footer.
// add_filter("gform_init_scripts_footer", "gform_footer_scripts");


// Login - prevent form from shaking.
// add_action('login_head', 'no_loginshake');


// MIME type - add custom MIME types.
add_filter('upload_mimes', 'custom_mime_types');


// DEBUG - flush rewrite.
// add_action( 'after_switch_theme', 'my_flush_rewrite_rules' );


?>