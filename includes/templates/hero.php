<?php
/*
 * Hero template.
 * Requires ACF.
 *
 */
?>


<?php if(get_field('hero_image')) : ?>
    <div class="m-hero" style="background-image: url('<?php the_field('hero_image') ?>');">
        <?php if(get_field('hero_title')) : ?>
            <h1 class=""><?php the_field('hero_title'); ?></h1>
        <?php endif; ?>
        <?php if(get_field('hero_content')) : ?>
            <div class="">
                <?php the_field('hero_content') ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>