<?php


// CSS conditional / vendor
function theme_css() {
    // wp_enqueue_style('icomoon', '', array(), '', 'screen');
}


// JS - custom js in footer.
function footer_scripts() {
    wp_register_script('fastclick', get_template_directory_uri() . '/assets/js/fastclick.js', array(), true);
    wp_register_script('main.js', get_template_directory_uri() . '/assets/js/main.js', array('jquery', 'fastclick'), true);
    wp_register_script('flexibility', get_template_directory_uri() . '/assets/js/flexibility.js', array(), true);

    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js', array('jquery', 'fastclick'), true);
    wp_enqueue_script('flexibility', get_template_directory_uri() . '/assets/js/flexibility.js', array('jquery', 'fastclick'), true);
}


// JS - Gravity Forms js in footer.
function gform_footer_scripts() {
    return true;
}


// Login - prevent form from shaking.
function no_loginshake() {
    remove_action('login_head', 'wp_shake_js', 12);
}


// MIME type - add custom MIME types.
function custom_mime_types ( $existing_mimes=array() ) {

    $existing_mimes['ai'] = 'application/postscript';

    $existing_mimes['eps'] = 'application/postscript';

    $existing_mimes['psd'] = 'application/octet-stream';

    $existing_mimes['svg'] = 'image/svg+xml';

    $existing_mimes['svgz'] = 'images/svg+xml';

    return $existing_mimes;
}


// Sidebar.
register_sidebar(array(
    'id' => 'sidebar-1',
    'name' => 'sidebar',
    'before_widget' => '<aside id="%1$s" class="m-sidebar__item %2$s">',
    'after_widget' => "</aside>",
    'before_title' => '<h2 class="m-sidebar__title">',
    'after_title' => '</h2>'
));


// DEBUG - flush rewrite.
function my_flush_rewrite_rules() {
    flush_rewrite_rules();
}


// Enable theme options via ACF.
if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Theme Header Settings',
        'menu_title'    => 'Social',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Theme Header Settings',
        'menu_title'    => 'Header',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Theme Footer Settings',
        'menu_title'    => 'Footer',
        'parent_slug'   => 'theme-general-settings',
    ));
}


?>