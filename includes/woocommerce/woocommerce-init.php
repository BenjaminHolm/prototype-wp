<?php


// Add to cart - global button text.
// add_filter('woocommerce_product_add_to_cart_text', 'woocommerce_custom_cart_button_text');


// Add to cart - single product button text.
// add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_single_custom_cart_button_text');


// Alert - remove coupon alert during checkout.
// remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10)


// Alert - remove login alert during checkout.
// remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10);


// Cart button - update dynamically.
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');


// Gallery.
// remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
// add_action('woocommerce_before_single_product_summary', 'woocommerce_product_gallery', 30);


// Notifications - remove quotation marks.
add_filter( 'wc_add_to_cart_message', 'woocommerce_remove_cart_quotes' );

// Placeholders.
add_filter( 'woocommerce_billing_fields', 'woocommerce_custom_billing_fields', 10);
add_filter( 'woocommerce_shipping_fields', 'woocommerce_custom_shipping_fields', 10);


// Plugin - remove increment styles.
// add_action( 'wp_print_styles', 'my_deregister_styles', 100 );


// Styles - remove default WooCommerce styles.
// add_filter( 'woocommerce_enqueue_styles', 'remove_woocommerce_styles' );


// Theme support.
add_action( 'after_setup_theme', 'woocommerce_support' );


// Wrapper - cart page.
// add_action('woocommerce_before_cart', 'woocommerce_before_cart_wrapper', 1);
// add_action('woocommerce_after_cart', 'woocommerce_after_cart_wrapper', 1);


// Wrapper - cart table.
// add_action('woocommerce_before_cart_table', 'woocommerce_before_cart_table_wrapper', 1);
// add_action('woocommerce_after_cart_table', 'woocommerce_after_cart_table_wrapper', 1);


// Wrapper - global WooCommerce.
// remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
// remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
// add_action('woocommerce_before_main_content', 'woocommerce_custom_wrapper_before', 20);
// add_action('woocommerce_after_main_content', 'woocommerce_custom_wrapper_after', 20);


?>