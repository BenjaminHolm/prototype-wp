<?php


// Add to cart - global button text.
function woocommerce_custom_cart_button_text() {
        return __( 'Add to Cart', 'woocommerce' );
}


// Add to cart - single product button text.
function woocommerce_single_custom_cart_button_text() {
        return __( 'Add to Cart', 'woocommerce' );
}


// Cart button - update dynamically.
function woocommerce_header_add_to_cart_fragment( $fragments ) {
  ob_start();
  ?>
  <a class="cart-contents" data-qty="<?php echo WC()->cart->cart_contents_count ?>" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
    <i class="icon-cart"></i>
    <span>Cart</span>
    <div data-qty="<?php echo WC()->cart->cart_contents_count ?>" id="cart-qty"><?php echo WC()->cart->cart_contents_count ?></div>
  </a>
  <?php

  $fragments['a.cart-contents'] = ob_get_clean();

  return $fragments;
}


// Gallery - single product page.
function woocommerce_product_gallery() {

}


// Notifications - remove quotation marks.
function woocommerce_remove_cart_quotes($message) {
    return str_replace(
    array(
      '&ldquo;', '&rdquo;'),
      '',
      $message
    );
}


// Plugin - remove increment styles.
// wordpress.org/plugins/woocommerce-quantity-increment
function my_deregister_styles() {
    wp_deregister_style( 'wcqi-css' );
}


// Placeholders - shipping fields.
function woocommerce_custom_shipping_fields($address_fields) {
    $address_fields['shipping_first_name']['placeholder'] = 'First Name';
    $address_fields['shipping_last_name']['placeholder'] = 'Last Name';
    $address_fields['shipping_company']['placeholder'] = 'Company';
    $address_fields['shipping_city']['placeholder'] = 'Town / City';
    $address_fields['shipping_postcode']['placeholder'] = 'Postal Code';
    return $address_fields;
}


// Placeholders - billing fields.
function woocommerce_custom_billing_fields($address_fields) {
    $address_fields['billing_first_name']['placeholder'] = 'First Name';
    $address_fields['billing_last_name']['placeholder'] = 'Last Name';
    $address_fields['billing_company']['placeholder'] = 'Company';
    $address_fields['billing_phone']['placeholder'] = 'Phone';
    $address_fields['billing_email']['placeholder'] = 'Email';
    $address_fields['billing_city']['placeholder'] = 'Town / City';
    $address_fields['billing_postcode']['placeholder'] = 'Postal Code';
    return $address_fields;
}


// Styles - remove default WooCommerce styles.
function remove_woocommerce_styles($enqueue_styles) {
    unset( $enqueue_styles['woocommerce-general']);    // Remove the gloss
    unset( $enqueue_styles['woocommerce-layout']);     // Remove the layout
    unset( $enqueue_styles['woocommerce-smallscreen']);    // Remove the smallscreen optimisation
    return $enqueue_styles;
}


// Theme support.
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


// Wrapper - cart page before.
function woocommerce_before_cart_wrapper() {
}


// Wrapper - cart page after.
function woocommerce_after_cart_wrapper() {
}


// Wrapper - cart table before.
function woocommerce_before_cart_table_wrapper() {
}


// Wrapper - cart table after.
function woocommerce_after_cart_table_wrapper() {
}


// Wrapper - global before.
function woocommerce_custom_wrapper_before() {
}


// Wrapper - global after.
function woocommerce_custom_wrapper_after() {
}


?>