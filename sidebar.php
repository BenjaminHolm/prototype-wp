<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
    <div class="m-blog__sidebar">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div>
<?php endif; ?>