<?php get_header(); ?>

    <div class="m-blog">
        <div class="m-blog__item">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <article class="m-article <?php get_post_class(); ?>">

                        <header class="m-article__header">
                            <h1 class="m-article__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                        </header>

                        <div class="m-article__meta">
                              <p><?php the_date('F j, Y') ?></p>
                        </div>

                        <div class="m-article__content">
                              <?php the_excerpt(); ?>
                              <p>
                                  <a href="<?php the_permalink(); ?>">Read More</a>
                              </p>
                        </div>


                    </article>

                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <div class="m-blog__sidebar">
            <?php dynamic_sidebar('sidebar-blog'); ?>
        </div>

    </div>
<?php get_footer(); ?>