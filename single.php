<?php get_header(); ?>

<main role="main">
    <div class="m-blog m-blog--single">
        <div class="m-blog__item m-blog__item--single" id="blog">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <article class="m-article m-article--single <?php get_post_class(); ?>">

                        <header class="m-article__header m-article__header--single">
                            <h1 class="m-article__title"><?php the_title(); ?></h1>
                        </header>

                        <div class="m-article__meta m-article__meta--single">
                              <p><?php the_date('F j, Y'); ?></p>
                        </div>

                        <div class="m-article__content m-article__content--single">
                              <?php the_content(); ?>
                        </div>

                        <?php wp_related_posts(); ?>

                    </article>

                <?php endwhile; ?>
            <?php endif; ?>

            <?php comments_template(); ?>
        </div>

        <div class="m-blog__sidebar m-blog__sidebar--single">
            <?php dynamic_sidebar('sidebar-blog'); ?>
        </div>

    </div>
</main>

<?php get_footer(); ?>